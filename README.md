## Simple rsync

Quick and dirty command launcher for nvim. Launches a set of defined
commands. These are defined in the `keys` option.


### Format

The `keys` section is a lua table where the key is the shortcut in combination
with the `prefix`. If the value is a:
- string    Interpreted as the command
- table     See table below for options, For the command, either [1] or `command` needs to be set
- function  The result is parsed as a value


### Table options
| option         | value (default)                   | description                       |
|----------------|-----------------------------------|-----------------------------------|
| command        | <command>                         | The command to run                |
| 1              | <command>                         | The command to run                |
| desc           | 'Run <first word of command>'     | The command description           |
| mode           | 'n' (string or table)             | The mode for the mapping          |
| useprefix      | true                              | Use the prefix                    |
| opts           | { nnoremap = true, silent = true} | Override `opts` to vim.keymap.set |


### Example / Installation
[lazy.nvim](https://github.com/folke/lazy.nvim):
```lua
-- lua/plugins/simple-cmd.lua

return {
    mode   = { 'n', 'v' },   -- Default: 'n'
    prefix = '<C-\>',        -- Default: '<leader>;'
    keys   = {
        gc = 'git commit',
        ap = 'git add -p',
        df = {
            command  = 'git diff "%file%"',
            desc     = 'Show diff of current buffer',
            noleader = true,
            mode     = 'v',
        },
    },
}
```

This would create these mappings:
- `<leader>;gc` in normal mode to open `git commit` with description of "Run git"
- `<leader>;ap` in normal mode to open `git add -p` with description of "Run git"
- `df` in visual mode to open `git diff "%file%"` with description "Show diff of current buffer"


### Variables
There are several variables to use in the command:
- %file%        The relative filename of the current buffer
- %file_full%   The fully qualified filename of the current buffer
- %file_path%   The fully qwualified path of the current buffer
- %file_name%   The filename of the current buffer
- %current_dir% The current working directory
