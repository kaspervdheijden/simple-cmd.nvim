local error_msg = function (msg)
    vim.api.nvim_err_writeln(msg)
end

local first_word = function (str)
    return string.match(str or '', '^([%w_.-]+)') or ''
end

local get_cmd = function (command)
    local variables = {
        file_path   = vim.fn.expand('%:p:h'),
        file_name   = vim.fn.expand('%:t'),
        file_full   = vim.fn.expand('%:p'),
        file        = vim.fn.expand('%'),
        current_dir = vim.fn.getcwd(),
    }

    for var, value in pairs(variables) do
        command = command:gsub('%%' .. var .. '%%', value)
    end

    return command
end

local on_exit = function (cmd, exit_code)
    if exit_code ~= 0 then
        error_msg('Command "' .. first_word(cmd) .. '" failed with exitcode ' .. exit_code)
    end

    vim.api.nvim_command('checktime')
end

return {
    setup = function (options)
        local opts   = options     or {}
        local prefix = opts.prefix or '<leader>;'

        for k, element in pairs(opts.keys or {}) do
            local item = type(element) == 'function' and element() or element
            local mode = opts.mode or 'n'
            local key  = prefix .. k
            local cmd  = nil
            local o    = {
                noremap = true,
                silent  = true,
            }

            if type(item) == 'table' then
                cmd = item.command or item[1]
                if cmd == nil then
                    error_msg('Invalid entry for "' .. k .. '"')
                end

                if item.mode ~= nil then
                    mode = item.mode
                end

                if type(item.opts) == 'table' then
                    o = item.opts
                end

                if (item.desc or '') ~= '' then
                    o.desc = item.desc
                end

                if not (item.useprefix or true) then
                    key = k
                end
            elseif (item or '') ~= '' then
                cmd = item
            end

            if (o.desc or '') == '' then
                o.desc = 'Run ' .. first_word(item)
            end

            if cmd ~= nil then
                vim.keymap.set(
                    mode,
                    key,
                    function ()
                        vim.fn.jobstart(get_cmd(cmd), { on_exit = on_exit })
                    end,
                    o
                )
            end
        end
    end
}
